#!/bin/bash

# apt-get install tree

# apt-get install emacs

emacs provision.sh

# apt-get install git

git config --global user.name "Javier Martinez"
git config --global user.email "javiliju@gmail.com"

# apt-get install wget

wget -O population.csv

# apt-get install grep

grep 'Spain'

# apt-get gnuplot

gnuplot set xlabel 'year'; sex ylabel 'population' set plot 'spain.tsv' using 2:3
