#!/bin/bash

set -x


# Collect


wget -O population.csv https://datahub.io/core/population/r/population.csv  

# Clean

cat population.csv | grep 'Spain' population.csv | cut -d',' -f1,3,4 | tail -n +2 | sed 's/,/\t/g' > spain.tsv  



# Visualize


cat spain.tsv | gnuplot -p -e "set terminal dumb; set xlabel 'year'; set ylabel 'population'; plot 'spain.tsv' using 2:3"
